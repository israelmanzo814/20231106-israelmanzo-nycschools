# 20231106-IsraelManzo-NYCSchools


## by Israel Manzo
### Characteristics:
_```XCode``` and ```SwiftUI```_

The application retrieves and displays a list of schools in a main feed. When a row is tapped, it navigates to a new detail view showcasing the SAT scores for each school.

**Architecture structure** _```swift MVVM```_.- This separation addresses concerns and assigns a **single responsibility** to each class. 
- Network layer takes care of the fetching and decoding of data coming from the _```api/internet```_ This was executed **concurrently** _```async/await```_ . 

- Model holds the object which the data will be store in memory
- The ViewModel converts data obtained from the network layer or the internet and, through its logic, establishes communication with the View where this data is presented to the user.
- The View, which is what the user sees and interacts with, showcases the processed data and presents what is available for the user to view.

**Passing Data Using Dependency Injection** .- Design pattern that aids in maintaining a clear separation of concerns. 

**Unit Testing:** Foundational tests carried out for the fetched and decoded data.

**Accessibility** is a vital aspect of ensuring that the application is inclusive for everyone. SwiftUI inherently provides a positive experience in this regard.

No third parties need it but Apple frameworks

### What I would improve.:
Conduct comprehensive and intricate _**Unit Testing**_.

Enhancement of _**UI/UX**_.

Enhancement _**Network Request**_.

Enhancement functionality such as search for school, detailed view for each school and scores, location map
