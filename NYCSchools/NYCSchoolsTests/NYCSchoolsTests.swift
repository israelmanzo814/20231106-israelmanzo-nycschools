//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Israel Manzo on 11/5/23.
//

import XCTest
@testable import NYCSchools

final class NYCSchoolsTests: XCTestCase {
    
    var sut: MockData!
    var vm: ViewModel!
    var mock: MockDataClass!

    @MainActor 
    override func setUpWithError() throws {
        sut = MockData()
        mock = MockDataClass()
        vm = ViewModel(services: NetworkServices())
    }

    override func tearDownWithError() throws {
        sut = nil
        mock = nil
    }
    
    // MARK: - Evaluating the mock object through testing.
    func testMockObjectData() async {
        let data = sut.object
        XCTAssertTrue(!data.isEmpty)
        XCTAssertFalse(data.isEmpty)
        XCTAssertTrue(data.count > 0)
    }
    
    // MARK: - Evaluating the fetch-decoded schools object during testing.
    func testGetDataWithFailure() async {
        await vm.getData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            let data = self.vm.schools
            if !data.isEmpty {
                XCTAssertNotNil(data)
            } else {
                XCTFail("Failed to fetch data")
            }
        }
    }
    
    // MARK: - Evaluating the decoded school scores object retrieved through fetch testing.
    func testGetScoresWithFailure() async {
        await vm.getScores()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            let scores = self.vm.satScores
            if !scores.isEmpty {
                XCTAssertNotNil(scores)
            } else {
                XCTFail("Failed to fetch scores")
            }
        }
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
