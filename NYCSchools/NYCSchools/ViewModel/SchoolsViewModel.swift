//
//  SchoolsViewModel.swift
//  NYCSchools
//
//  Created by Israel Manzo on 11/3/23.
//

import SwiftUI

// MARK: - The ViewModel encapsulates the logic responsible for retrieving and decoding data before making it accessible to the view.
// MainActor is a globally unique actor who performs his tasks on the main thread
@MainActor
final class ViewModel: ObservableObject {
    
    @Published private(set) var schools = [NYCSchool]()
    private(set) var satScores = [SATScores]()
    @Published private(set) var errorMessage = ""
    private let services: NetworkServices
    
    init(services: NetworkServices) {
        self.services = services
    }
    
    // MARK: - Retrieve the list of schools.
    func getData() async {
        do {
            self.schools = try await services.fetchData(from: Constant.schoolListEndpoint)
            await getScores()
        } catch {
            print("DEBUG: - Error : \(APIError.ErrorGettinggData(error.localizedDescription)) - ")
            self.errorMessage = error.localizedDescription
        }
    }
    
    // MARK: - Retrieve the list of scores.
    func getScores() async {
        do {
            self.satScores = try await services.fetchData(from: Constant.schoolSATResult)
            self.mapSATScores(with: satScores)
        } catch {
            print("DEBUG: - Error : \(APIError.ErrorGettingSATScores(error.localizedDescription)) - ")
        }
    }
    
    // MARK: - Comparing the unique identifiers for each school from the school list against the corresponding scores in the scores list.
    private func mapSATScores(with scores: [SATScores]) {
        let previous = self.schools
        self.schools.removeAll() // re-start school each time is called
        
        scores.forEach { score in
            if let dbn = score.dbn {
                var matchedSchool = previous.first(where: { $0.dbn == dbn }) // finding the dbn "id"
                
                guard matchedSchool != nil else { return }
                
                matchedSchool?.satScores = score
                
                guard let matchedSchool = matchedSchool else { return }
                
                self.schools.append(matchedSchool) // returning an array of matched schools
            }
        }
    }
}
