//
//  Constants.swift
//  NYCSchools
//
//  Created by Israel Manzo on 11/3/23.
//

import Foundation

struct Constant {
    static let schoolListEndpoint = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let schoolSATResult = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}
