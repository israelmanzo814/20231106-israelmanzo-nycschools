//
//  NetworkServices.swift
//  NYCSchools
//
//  Created by Israel Manzo on 11/3/23.
//

import SwiftUI

// MARK: - Implement error handling using an enumeration for the fetch request method.
enum APIError: Error {
    case badURL
    case badResponse
    case ErrorGettinggData(_ message: String)
    case ErrorGettingSATScores(_ message: String)
}

// MARK: - Protocol blueprint for network request
protocol NetworkServicesProtocol {
    func fetchData<T: Codable>(from urlString: String) async throws -> [T]
}

// MARK: - Create a fetch request component utilizing async/await.
/*
 - Retrieve the URL.
 - Initiate a URLSession for the URL.
 - Process the response, ensuring it falls within the 200 to 300 range.
 - Return the decoded data.
 */
final class NetworkServices: NetworkServicesProtocol {
    
    func fetchData<T: Codable>(from urlString: String) async throws -> [T] {
        guard let url = URL(string: urlString) else {
            throw APIError.badURL
        }
        let (data, response) = try await URLSession.shared.data(from: url)
        
        guard let response = response as? HTTPURLResponse,
                (200...300).contains(response.statusCode) else {
            throw APIError.badResponse
        }
        
        return try JSONDecoder().decode([T].self, from: data)
    }
}


// MARK: - Mock object - returns three objects when retrieving mock data.
struct MockData {
    let object: [NYCSchool] = [
        NYCSchool(dbn: "0", schoolName: "John Kennedy", city: "New York", stateCode: "NY", primaryAddress: "10 East 15th Street"),
        NYCSchool(dbn: "1", schoolName: "Bayside", city: "College Point", stateCode: "NY", primaryAddress: "250 19th Street"),
        NYCSchool(dbn: "2", schoolName: "Tom Parker", city: "Brooklyn", stateCode: "NY", primaryAddress: "20 15th Avenue")
    ]
    
}

class MockDataClass {
    var mockData = [NYCSchool]()
    func getMockData() async {
        self.mockData = MockData().object
    }
}
