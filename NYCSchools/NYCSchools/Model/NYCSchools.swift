//
//  NYCSchools.swift
//  NYCSchools
//
//  Created by Israel Manzo on 11/3/23.
//

import SwiftUI

struct NYCSchool: Codable {
    let dbn: String
    let schoolName: String
    let city: String
    let stateCode: String
    let primaryAddress: String
    
    var satScores: SATScores?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case city
        case stateCode = "state_code"
        case primaryAddress = "primary_address_line_1"
    }
}

struct SATScores: Codable {
    let dbn: String?
    let schoolName: String?
    let numberOfSATTestTakers: String?
    let satCriticalReadingAverageScore: String?
    let satMathAverageScore: String?
    let satWritingAverageScore: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numberOfSATTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAverageScore = "sat_critical_reading_avg_score"
        case satMathAverageScore = "sat_math_avg_score"
        case satWritingAverageScore = "sat_writing_avg_score"
    }
}
