//
//  SchoolDetailView.swift
//  NYCSchools
//
//  Created by Israel Manzo on 11/3/23.
//

import SwiftUI

struct SchoolDetailView: View {
    var school: NYCSchool
    var body: some View {
        VStack {
            VStack(alignment: .leading) {
                Text(school.schoolName)
                    .font(.title)
                    .foregroundStyle(.primary)
                Text("Number of SAT test takers Averrage")
                    .font(.headline)
                    .foregroundStyle(.secondary)
                VStack(alignment: .leading) {
                    Text("Students: \(school.satScores?.numberOfSATTestTakers ?? "")")
                    Text("Math Averrage")
                        .foregroundStyle(.secondary)
                    Text("score: \(school.satScores?.satCriticalReadingAverageScore ?? "")")
                    Text("Math Average")
                        .foregroundStyle(.secondary)
                    Text("score: \(school.satScores?.satMathAverageScore ?? "")")
                    Text("Writing average")
                        .foregroundStyle(.secondary)
                    Text("score: \(school.satScores?.satWritingAverageScore ?? "")")
                }
                .font(.callout)
                .fontWeight(.medium)
                
            }
            .fontWeight(.light)
            .padding()
            .frame(maxWidth: .infinity, alignment: .leading)
        }
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.sRGB, red: 15/255, green: 15/255, blue: 15/255, opacity: 0.1), lineWidth: 1.5)
        )
        .padding(.horizontal, 30)
        HStack {
            Spacer()
            Text("\(school.primaryAddress), \(school.city), \(school.stateCode)")
                .font(.headline)
                .foregroundStyle(.secondary)
        }
        .padding(.horizontal, 30)
        
        Spacer()
    }
}

#Preview {
    SchoolDetailView(school: NYCSchool(dbn: "", schoolName: "John Kennedy", city: "New York", stateCode: "NY", primaryAddress: "10 East 15th Street", satScores: SATScores(dbn: "", schoolName: "", numberOfSATTestTakers: "12", satCriticalReadingAverageScore: "10", satMathAverageScore: "5", satWritingAverageScore: "70")))
}
