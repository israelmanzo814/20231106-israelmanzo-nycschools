//
//  ContentView.swift
//  NYCSchools
//
//  Created by Israel Manzo on 11/2/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        MainListView()
    }
}

#Preview {
    ContentView()
}
