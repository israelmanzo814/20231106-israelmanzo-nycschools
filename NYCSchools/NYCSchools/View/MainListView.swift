//
//  MainListView.swift
//  NYCSchools
//
//  Created by Israel Manzo on 11/2/23.
//

import SwiftUI

struct MainListView: View {
    
    @StateObject private var vm: ViewModel
    
    init() {
        self._vm = StateObject(wrappedValue: ViewModel(services: NetworkServices()))
    }
    
    var body: some View {
        NavigationView {
            List {
                if vm.schools.isEmpty {
                    HStack {
                        ProgressView()
                        Text("Loading...")
                            .padding()
                            .foregroundColor(Color(.systemGray))
                    }
                }
                ForEach(vm.schools, id: \.dbn) { school in
                    NavigationLink {
                        SchoolDetailView(school: school)
                    } label: {
                        SchoolRowView(school: school)
                    }
                }
            }
            .task {
                await vm.getData()
//                let _ = print(await vm.getData())
            }
            .navigationTitle("NYC Schools")
        }
    }
}

#Preview {
    MainListView()
}

struct SchoolRowView: View {
    var school: NYCSchool
    var body: some View {
        VStack(alignment: .leading) {
            Text(school.schoolName)
                .font(.title2)
                .fontWeight(.light)
            HStack {
                Text("\(school.city), \(school.stateCode)")
                    .font(.callout)
                    .fontWeight(.bold)
            }
        }
        
    }
}


