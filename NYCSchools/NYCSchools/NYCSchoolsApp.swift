//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by Israel Manzo on 11/2/23.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
